﻿var express = require('express');
var router = express.Router();
var mysql = require('mysql');
var formidable = require('formidable');

var connection = mysql.createConnection({
	host: 'localhost',
	user: 'uporabnik',
	password: 'uporabnik',
	database: 'osebe'
});

try {
	connection.connect();
	console.log("Database \'osebe\' connected!");
} catch (err) {
	console.log(err);
}

var person = {
	ID_OSB: 666,
	IME_OSB: 'test',
	TELEFON: 'drugo'
};

router.get('/', function (zahteva, odgovor, next) {
	
	connection.query('SELECT * FROM imenik_osb ORDER BY IME_OSB', function (err, rows, fields) {
		if (!err) {
			//console.log("Vrstice: ", rows[0]);
			person = rows;
			//console.log("Oseba: ", person);
			odgovor.render('phonebook', { oseba: person });
		} else {
			console.log("Napakaaa!", err);
		}
	});
	
});

router.get('/edit_person/:id', function (req, res, next) {
	var id = req.params.id;
	var sql = 'SELECT * FROM IMENIK_OSB WHERE ID_OSB=' + id;
	connection.query(sql, function (err, rows, fields) {
		if (!err) {
			person = rows[0];
			res.render('edit_person', { oseba: person });
		} else {
			console.log("Napaka: ", err);
		}
	});
});

router.get('/delete_person/:id', function (req, res, next) {
	var id = req.params.id;
	var sql = "DELETE FROM IMENIK_OSB WHERE ID_OSB=" + id;
	connection.query(sql);
	res.redirect('/phonebook/');
});

/*INSERT INTO `imenik_osb` (`ID_OSB`, `IME_OSB`, `TELEFON`, `ULICA`, `POS_ST`, `POSTA`,
 *  `DRZAVA`, `E_MAIL`) VALUES (NULL, 'test', '5555', 'test', '455', 'test', 'test', 'test@test')*/

router.post('/dodaj', function (req, res, next) {
	var form = new formidable.IncomingForm();
	form.parse(req, function (err, fields, files) {
		if (!err) {
			var sql = "INSERT INTO IMENIK_OSB (ID_OSB, IME_OSB, TELEFON, ULICA, POS_ST, POSTA, DRZAVA, E_MAIL) VALUES (NULL, '" +
						fields.Name + "', '" + fields.Telefon + "', '" + fields.Ulica + "', '" + fields.Pos_st + "', '" + fields.Posta +
						"', '" + fields.Drzava + "', '" + fields.Email + "')";
			//console.log("SQL: ", sql);
			connection.query(sql);
			res.redirect('/phonebook/');
 		} else {
			console.log("NAPAKA: ", err);
		}
	});
});

//UPDATE `imenik_osb` SET `ULICA` = 'test' WHERE `imenik_osb`.`ID_OSB` = 3 
//To edit
router.post('/save_person/:id', function (req, res, next) {
	var id = req.params.id;
	console.log("urejam osebo: ", id);
	var form = new formidable.IncomingForm();

	form.parse(req, function (err1, fields, files) {
		if (!err1) {
			
			var sql = "UPDATE IMENIK_OSB SET " +
				"IME_OSB='" + fields.Name +
				"', TELEFON='" + fields.Telefon +
				"', ULICA='" + fields.Ulica +
				"', POS_ST='" + fields.Pos_st +
				"', POSTA='" + fields.Posta +
				"', DRZAVA='" + fields.Drzava +
				"', E_MAIL='" + fields.Email + 
				"' WHERE ID_OSB=" + id;
			//console.log("SQL : ", sql);
			connection.query(sql);
			res.redirect('/phonebook/oseba/' + id);
		} else {
			console.log("NAPAKAAAAAAAA*****!*!!");
		}
	});


});

router.get('/oseba/:id', function (req, res, next){
	var id = req.params.id;
	console.log("ZAHTEVA: /oseba/", id);
	var sql = 'SELECT * FROM IMENIK_OSB WHERE ID_OSB=' + id;
	connection.query(sql, function (err, rows, fields) {
		if (!err) {
			person = rows[0];
			//console.log(person);
			res.render('oseba', { oseba: person });
		} else {
			console.log("Napaka: ", err);
		}
	});
});




module.exports = router;
