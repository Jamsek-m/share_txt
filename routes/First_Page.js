﻿var express = require('express');
var router = express.Router();

/* GET first_page */
router.get('/', function (req, res, next) {
	res.render('First_Page', {title:'ASCII Art'});
});

module.exports = router;
