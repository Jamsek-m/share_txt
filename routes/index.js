var express = require('express');
var passport = require('passport');
var flash = require('connect-flash');
var morgan = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var mysql = require('mysql');


var router = express.Router();
//app.use(cookieParser());



var connection = mysql.createConnection({
	host: 'localhost',
	user: 'uporabnik',
	password: 'uporabnik',
	database: 'osebe'
});

try {
	connection.connect();
	//console.log("Database \'osebe\' connected!");
} catch (err) {
	console.log("Napaka pri povezovanju v bazo: ", err);
}


router.get('/login', function (req, res, next) {
	res.render('login');
});

router.post('/prijavaIn', function (req, res, next) {
	res.send("Uspelo!");
});


/* GET home page. */
router.get('/', function (req, res, next) {
	connection.query('SELECT * FROM share_txt_uporabniki', function (err, rows, fields) {
		if (!err) {
			console.log("Vrstice: ", rows);
			res.render('index', { title: 'Kratki projekti' });		
		} else {
			console.log("Napaka pri poizvedbi! ", err);
		}
	});
});

module.exports = router;
